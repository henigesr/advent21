aim = 0
horiz = 0
depth = 0

with open("../inputs/input2.txt", 'r') as f:
    directions = [i for i in f.read().splitlines()]

for direction in directions:
    orientation, distance = direction.split()
    if orientation == 'up':
        aim -= int(distance)
    elif orientation == 'down':
        aim += int(distance)
    elif orientation == 'forward':
        horiz += int(distance)
        depth += (int(aim) * int(distance))

print(horiz)
print(depth)
print(horiz * depth)