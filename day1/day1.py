with open("input.txt", "r") as text_input:

    num_list = text_input.read().split("\n")

count = 0

for n in range(0, len(num_list)-3):
    window_1 = (num_list[n] + num_list[n+1] + num_list[n+2])
    window_2 = (num_list[n+1] + num_list[n+2] + num_list[n+3])

    if num_list[n+3] > num_list[n]:
        count += 1

print(count)
print(len(num_list))