with open("./input4.txt", 'r') as f:
    all_numbers = f.readline().split(",")

    board_lists = []
    row_list = []
    for line in f.readlines():
        if line == "\n":
            if row_list:
                board_lists.append(row_list)
            row_list = []
        else:
            row = [value.replace('\n', '') for value in line.split(' ') if value != ""]
            row_list.append(row)

board_lists.append([['83', '7', '80', '86', '65'],
['37', '22', '19', '84', '92'],
['29', '17', '76', '4', '33'],
['97', '50', '1', '12', '21'],
['15', '58', '39', '38', '74']])


def check_rows(board, drawn_numbers):
    for board_row in board:
        winner_count = 0
        for board_number in board_row:
            if board_number in drawn_numbers:
                winner_count += 1
                if winner_count == 5:
                    return True
                continue
            else:
                break
    return False


def check_columns(board, drawn_numbers):
    columns = [0, 1, 2, 3, 4]
    for column in columns:
        winner_count = 0
        for board_row in board:
            if board_row[column] in drawn_numbers:
                winner_count += 1
                if winner_count == 5:
                    return True
                continue
            else:
                break
    return False


def winner_function(winning_board, drawn_nums):
    print("WINNER")
    unmarked_sum = 0

    for board_row in winning_board:
        for num in board_row:
            if num not in drawn_nums:
                unmarked_sum += int(num)

    submission = unmarked_sum * int(drawn_nums[len(drawn_nums)-1])
    print(submission)
    exit()


drawn_numbers = []
winning_boards = []
winners = 0

for bingo_number in all_numbers:
    drawn_numbers.append(bingo_number)

    for board_index in range(0, len(board_lists)-1):
        try:
            row_win = check_rows(board_lists[board_index], drawn_numbers)
            column_win = check_columns(board_lists[board_index], drawn_numbers)
        except IndexError:
            continue

        if (row_win or column_win):
            del board_lists[board_index]
            if len(board_lists) == 1:
                print("Gottem")
                print(board_lists[0])
                winner_function(board_lists[0], drawn_numbers)

        elif len(board_lists) == 1:
            print(board_lists)

print("Through all nums")