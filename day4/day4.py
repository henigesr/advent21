with open("./input4.txt", 'r') as f:
    all_numbers = f.readline().split(",")

    board_lists = []
    row_list = []
    for line in f.readlines():
        if line == "\n":
            if row_list:
                board_lists.append(row_list)
            row_list = []
        else:
            row = [value.replace('\n', '') for value in line.split(' ') if value != ""]
            row_list.append(row)


def winner_function(winning_board, drawn_nums):
    print("WINNER")
    unmarked_sum = 0

    for board_row in winning_board:
        for num in board_row:
            if num not in drawn_nums:
                unmarked_sum += int(num)

    submission = unmarked_sum * int(drawn_nums[len(drawn_nums)-1])
    print(submission)
    exit()


def check_rows(board, drawn_numbers):
    for board_row in board:
        winner_count = 0
        for board_number in board_row:
            if board_number in drawn_numbers:
                winner_count += 1
                if winner_count == 5:
                    print(drawn_numbers)
                    winner_function(board, drawn_numbers)
                continue
            else:
                print("row lose")
                break


def check_columns(board, drawn_numbers):
    columns = [0, 1, 2, 3, 4]
    for column in columns:
        winner_count = 0
        for board_row in board:
            if board_row[column] in drawn_numbers:
                winner_count += 1
                if winner_count == 5:
                    print(drawn_numbers)
                    winner_function(board, drawn_numbers)
                continue
            else:
                print("column lose")
                break


drawn_numbers = []

for bingo_number in all_numbers:
    drawn_numbers.append(bingo_number)
    for board in board_lists:
        check_rows(board, drawn_numbers)
        check_columns(board, drawn_numbers)

