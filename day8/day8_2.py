def read_file():

    with open('./input8.txt', 'r') as f:
        seven_segment_digit_entries = f.readlines()

    return seven_segment_digit_entries


def main():
    seven_segment_digit_entries = read_file()

    tot_sum = 0

    for seven_segment_digit_entry in seven_segment_digit_entries:
        signal_patterns = seven_segment_digit_entry.split(" | ")[0].replace('\n', '').split(" ")
        output_values = seven_segment_digit_entry.split(" | ")[1].replace('\n', '').split(" ")

        known_nums = {}

        while len(known_nums) != 10:  # Go until all 10 digits have been discovered

            for pattern in signal_patterns:  # Start with numbers that have unique segment count
                if len(pattern) == 2:
                    known_nums[1] = pattern
                elif len(pattern) == 4:
                    known_nums[4] = pattern
                elif len(pattern) == 3:
                    known_nums[7] = pattern
                elif len(pattern) == 7:
                    known_nums[8] = pattern

            for pattern in signal_patterns:
                if len(pattern) == 6:  # Must be 0 6 or 9
                    if len(set(pattern) - set(known_nums[1])) == 5:
                        known_nums[6] = pattern
                    elif len(set(pattern) - set(known_nums[4])) == 2:
                        known_nums[9] = pattern
                    else:
                        known_nums[0] = pattern

            for pattern in signal_patterns:
                if len(pattern) == 5:  # Must be 2 3 or 5
                    if len(set(pattern) - set(known_nums[7])) == 2:
                        known_nums[3] = pattern
                    elif len(set(pattern) - set(known_nums[4])) == 3:
                        known_nums[2] = pattern
                    else:
                        known_nums[5] = pattern

        # 1) Loop through each output digit in the current entry
        # 2) For each, find which known_value is equivalent
        # 3) Append that number to that entry's output value
        # 4) Add to total sum

        # Initialize output string
        output_string = ''

        # Step 1
        for output_value in output_values:

            # Step 2
            decoded_num = str(next((key for key, value in known_nums.items() if set(value) == set(output_value)), None))

            # Step 3
            output_string += decoded_num

        print(output_string)
        tot_sum += int(output_string)

        # print(known_nums)
        # print(len(known_nums))

    print(tot_sum)


main()
