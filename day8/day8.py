def read_file():

    with open('./input8.txt', 'r') as f:
        seven_segment_digit_entries = f.readlines()

    return seven_segment_digit_entries


def main():
    seven_segment_digit_entries = read_file()

    count = 0

    for seven_segment_digit_entry in seven_segment_digit_entries:
        signal_patterns = seven_segment_digit_entry.split(" | ")[0].replace('\n', '').split(" ")
        output_values = seven_segment_digit_entry.split(" | ")[1].replace('\n', '').split(" ")

        for output_value in output_values:
            if len(output_value) in [2, 3, 4, 7]:
                count += 1

    print(count)

main()
