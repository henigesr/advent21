from timeit import default_timer as timer
# https://adventofcode.com/2020/day/10

# joltage adapters - can take input 1, 2, or 3 jolts lower than it's rating
# the device - has built in adapter is 3 higher than the highest-rated adapter in the bag
# charging outlet has joltage rating of 0

start = timer()


def open_file():  # Returns the file contents as a sorted list of ints
    with open("input10.txt", "r") as f:
        adapters = [int(adapters) for adapters in f.read().split('\n')]

    adapters.sort()  # Sort to easily find max value

    return adapters


def main():
    # Variables to count 1 and 3 jolt differences for part 1 solution
    jolt_diffs = [0, 0, 0]

    adapters = open_file()

    output_joltage = 0  # Starting with the charging outlet's effective joltage of 0
    device_adapter = adapters[-1] + 3  # Determine device's built-in joltage adapter

    adapters.append(device_adapter)

    for adapter in adapters:
        jolt_diffs[adapter - output_joltage - 1] += 1
        output_joltage = adapter

    adapters.reverse()  # Work from the end of the chain "back" towards the beginning
    adapters.append(0)  # Make sure the "wall charger" is in the list

    # A second list will record the possible connections for each adapter, matching based on list index
    possible_connections = []

    for index, adapter in enumerate(adapters):  # Loop for part 2
        # Starting from the end of the chain and working backwards:
        # For a given adapter, what adapters can it connect to?
        # For that given adapter, the possible paths from it to the device is the sum of the possible paths of the
        # adapters that it can connect to.

        possible_connections.append(0)  # Preparing for later adding value to list based on index

        if index == 0:
            # While the "device" doesn't have a possible connection, it needs to start with a value of 1 since we're
            # just summing up previous values, otherwise it would just stay at 0
            possible_connections[0] = 1
            continue

        for i in range(1, 4):  # Only need to check the previous 3 list items
            if index - i >= 0:  # Don't loop back around to the other end of the list

                # For an adapter, which of the three previous elements in
                # the list are 1, 2, or 3 more than it?
                if adapters[index - i] <= adapter + 3:

                    possible_connections[index] += possible_connections[index - i]


    print(f"Part 1: {jolt_diffs[0] * jolt_diffs[2]}")

    print(f"Part 2: {possible_connections[-1]}")


main()

end = timer()

print(f"Time elapsed: {end - start}")
