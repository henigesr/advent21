def get_line_segments():

    line_segments = []

    with open("./input5.txt", 'r') as f:
        f_lines = f.readlines()
        for line in f_lines:
            cords = line.split(" ")
            x1, y1 = cords[0].split(',')
            x2, y2 = cords[2].split(',')

            line_segments.append([int(x1), int(y1), int(x2), int(y2)])

        return line_segments


def overlapping_point_counter(count_per_point):
    count = 0

    for _ in enumerate(count_per_point.items()):
        if _[1][1] > 1:
            count += 1

    return count


def main():
    all_line_segments = get_line_segments()

    count_per_point = {}

    for line_segment in all_line_segments:
        x1, y1, x2, y2 = line_segment[0], line_segment[1], line_segment[2], line_segment[3]

        if x1 == x2:
            for i in range(min(y1, y2), max(y1, y2)+1):
                try:
                    count_per_point[(x1, i)] += 1
                except KeyError:
                    count_per_point[(x1, i)] = 1
        elif y1 == y2:
            for i in range(min(x1, x2), max(x1, x2)+1):
                try:
                    count_per_point[(i, y1)] += 1
                except KeyError:
                    count_per_point[(i, y1)] = 1
        elif (max(x1, x2) - min(x1, x2)) and (max(y1, y2) - min(y1, y2)):
            # if both of one set are the minimum, it scales up, otherwise based on min x increase, max y decreases
            if (min(x1, x2) == x1) and (min(y1, y2) == y1):
                for i in range(0, (max(x1, x2) - min(x1, x2)) + 1):
                    x = x1 + i
                    y = y1 + i
                    try:
                        count_per_point[(x, y)] += 1
                    except KeyError:
                        count_per_point[(x, y)] = 1
            elif (min(x1, x2) == x2) and (min(y1, y2) == y2):
                for i in range(0, (max(x1, x2) - min(x1, x2))+1):
                    x = x2 + i
                    y = y2 + i
                    try:
                        count_per_point[(x, y)] += 1
                    except KeyError:
                        count_per_point[(x, y)] = 1
            elif (min(x1, x2) == x1) and (max(y1, y2) == y1):
                for i in range(0, (max(x1, x2) - min(x1, x2))+1):
                    x = x1 + i
                    y = y1 - i
                    try:
                        count_per_point[(x, y)] += 1
                    except KeyError:
                        count_per_point[(x, y)] = 1
            elif (min(x1, x2) == x2) and (max(y1, y2) == y2):
                for i in range(0, (max(x1, x2) - min(x1, x2))+1):
                    x = x2 + i
                    y = y2 - i
                    try:
                        count_per_point[(x, y)] += 1
                    except KeyError:
                        count_per_point[(x, y)] = 1

    answer = overlapping_point_counter(count_per_point)
    print(answer)


main()
