def get_starting_fish():
    starting_fish_counts = {}
    with open('./input6.txt', 'r') as f:
        starting_fish = [int(i) for i in f.readline().split(',')]

        for timer_value in range(0, 9):
            starting_fish_counts[timer_value] = starting_fish.count(timer_value)

    return starting_fish_counts


def main():
    days = 256
    all_fish = get_starting_fish()

    for day in range(0, days):
        new_fish_timers = {}
        for timer_value in range(0, 8):  # Moves 1 through 8 down by one value
            new_fish_timers[timer_value] = all_fish[timer_value+1]
        new_fish_timers[8] = all_fish[0]
        new_fish_timers[6] += all_fish[0]
        all_fish = new_fish_timers
        # All value counts decrease by one, 0 duplicates to both 8 and 6

    fish_sum = 0
    for value in all_fish.values():
        fish_sum += value

    print(fish_sum)

main()
