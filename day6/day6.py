def get_starting_fish():
    with open('./input6.txt', 'r') as f:
        starting_fish = [int(i) for i in f.readline().split(',')]
        return starting_fish



def main():
    days = 256
    all_fish = get_starting_fish()

    for day in range(0, days):
        for index in range(0, len(all_fish)):
            if all_fish[index] == 0:
                all_fish[index] = 6
                all_fish.append(8)
            else:
                all_fish[index] -= 1

    print(len(all_fish))


main()