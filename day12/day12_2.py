def open_solutions():
    with open('example_solution.txt', 'r') as file:
        connections = file.read().split("\n")

    list_solutions = []

    for connection in connections:
        new_list = connection.split(',')
        list_solutions.append(new_list)
        print(new_list)

    return list_solutions


def open_file():
    with open('input12.txt', 'r') as file:
        connections = file.read().split("\n")

    return connections


def make_connections_dict(connections):
    connections_dict = {}
    for connection in connections:
        start_point, end_point = connection.split('-')
        try:
            connections_dict[start_point].append(end_point)
        except KeyError:
            if end_point != 'start':
                connections_dict[start_point] = [end_point]

        try:
            connections_dict[end_point].append(start_point)
        except KeyError:
            if start_point != 'start' and end_point != 'end':
                connections_dict[end_point] = [start_point]
    return connections_dict


def eligible_paths(path, connections_dict):
    # If there has been a small cave visited twice, then only upper case caves and unvisited lower case caves
    # are eligible. Otherwise, all caves are eligible
    eligible_paths_list = []
    # print(f"Path is {path}")

    # First determine if a small cave has been visited twice
    visited_small_caves = [elem for elem in path if elem.islower()]
    visited_set = list( dict.fromkeys(visited_small_caves))

    if len(visited_small_caves) >= (len(visited_set) + 1):  # The same logic from pt 1 can apply
        for cave in connections_dict[path[-1]]:
            if cave.islower() and cave in path:
                pass
            else:
                eligible_paths_list.append(path + [cave])
        return eligible_paths_list
    else:
        for cave in connections_dict[path[-1]]:
            if cave != 'start':
                eligible_paths_list.append(path + [cave])
        return eligible_paths_list


def get_lower_case_caves(connections_dict):
    lower_caves = []
    for elem in connections_dict.keys():
        if elem.islower() and elem != 'start':
            lower_caves.append(elem)
    return lower_caves


def main():
    connections = open_file()
    connections_dict = make_connections_dict(connections)
    complete_paths = []
    possible_paths = []
    lower_case_caves = get_lower_case_caves(connections_dict)

    for cave in connections_dict['start']:
        # Finding intial paths since eligible_paths wants lists at least 2 long
        possible_paths.append(['start', cave])

    # while possible_paths:
    while possible_paths:
        # 1) If any are complete (start - ... - end) store in a finished list
        # 2) Determine eligible next steps
        #   a) If there are none, remove this path
        #   b) Else, append to the list each eligible next step (full path)
        for possible_path in possible_paths:
            # print(f"Possible path {possible_path} is being evaluated")
            # Iterate through all possible paths.
            if possible_path[0] == 'start' and possible_path[-1] == 'end':
                # This path is complete, add to complete paths and remove from possible paths
                complete_paths.append(possible_path)
                possible_paths.remove(possible_path)
            else:
                # Otherwise, determine if it's a dead end
                possible_path_next_paths = eligible_paths(possible_path, connections_dict)

                if possible_path_next_paths == []:
                    # Nowhere else to go, dead end
                    possible_paths.remove(possible_path)
                else:
                    # Add the next steps to possible_paths and remove the previous possible path
                    # for path in possible_path_next_paths:
                    possible_paths = possible_paths + possible_path_next_paths
                    possible_paths.remove(possible_path)

    print(len(complete_paths))


main()
