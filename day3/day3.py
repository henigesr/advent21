# 011011001101

with open("../inputs/input3.txt", 'r') as f:
    report = [i for i in f.read().splitlines()]

gamma_rate = ""
epsilon_rate = ""
oxygen_rating = ""
co2_rating = ""

# Given a certain position and a list of binary numbers, find and return the count of 0s and 1s in that position
def count_by_position(pos, binary_nums):
    zero_count = 0
    one_count = 0
    for num in binary_nums:
        if num[pos] == '0':
            zero_count += 1
        elif num[pos] == '1':
            one_count += 1

    return([zero_count, one_count])

def eliminate_by_position_value(pos, value, binary_nums):
    if len(binary_nums) == 1:
        return binary_nums
    else:
        trimmed_list = [num for num in binary_nums if num[pos] == value]
        return(trimmed_list)

oxygen_report = report
co2_report = report

# def get_criteria_num(given_report):
#     for position in range(0, len(given_report[0])):
#         counts = count_by_position(position, given_report)
#         print(counts)
#         if counts[0] > counts[1]:
#             given_report = eliminate_by_position_value(position, "0", given_report)
#         elif counts[0] <= counts[1]:
#             oxygen_report = eliminate_by_position_value(position, "1", given_report)
#         else:
#             print("ERROR")


for position in range(0, len(report[0])):
    counts = count_by_position(position, co2_report)
    print(counts)
    if counts[0] > counts[1]:
        oxygen_report = eliminate_by_position_value(position, "0", oxygen_report)
        co2_report = eliminate_by_position_value(position, "1", co2_report)
    elif counts[0] <= counts[1]:
        oxygen_report = eliminate_by_position_value(position, "1", oxygen_report)
        co2_report = eliminate_by_position_value(position, "0", co2_report)
    else:
        print("ERROR")


print(oxygen_report)
print(co2_report)


# for position in range(0, len(report[0])):
#     counts = count_by_position(position, report)
#     if counts[0] > counts[1]:
#         gamma_rate += "0"
#         epsilon_rate += "1"
#     elif counts[0] < counts[1]:
#         gamma_rate += "1"
#         epsilon_rate += "0"
#
# print(f"{gamma_rate} {epsilon_rate}")
# print(int(gamma_rate, 2))
# print(int(epsilon_rate, 2))
#
# power = int(gamma_rate, 2) * int(epsilon_rate, 2)
#
# print(power)
