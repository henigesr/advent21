import pandas as pd

def rewrite_file():
    with open('./input9.txt', 'r') as f:
        lines = f.read().split("\n")
    newlines = []
    for line in lines:
        newline = ''
        for num in line:
            newline += num + ','
        # print(newline)
        newlines.append(newline[:-1])

    with open('./input9.csv', 'w') as newfile:
        newfile.write("\n".join(newlines))

rewrite_file()

def read_file():

    heightmap_array = pd.read_csv('./input9.csv', header=None)

    return heightmap_array


def main():

    risk_level = 0

    heightmap_array = read_file()

    row_index = len(heightmap_array)
    column_index = len(heightmap_array.columns)

    for current_row in range(0, row_index):
        for current_column in range(0, column_index):

            is_low_point = True

            current_value = heightmap_array.iat[current_row, current_column]

            if current_row == 0:
                # print("Top row")
                pass
            else:
                north_value = heightmap_array.iat[current_row-1, current_column]
                if current_value >= north_value:
                    is_low_point = False
                # print(f"N: {north_value}")

            try:
                south_value = heightmap_array.iat[current_row+1, current_column]
                if current_value >= south_value:
                    is_low_point = False
                # print(f"S: {south_value}")
            except IndexError:
                # print("Bottom row")
                pass

            if current_column == 0:
                # print("Left column")
                pass
            else:
                west_value = heightmap_array.iat[current_row, current_column-1]
                if current_value >= west_value:
                    is_low_point = False
                # print(f"W: {west_value}")

            try:
                east_value = heightmap_array.iat[current_row, current_column+1]
                if current_value >= east_value:
                    is_low_point = False
                # print(f"E: {east_value}")
            except IndexError:
                # print("Right column")
                pass

            if is_low_point == True:
                risk_level += int(current_value) + 1

    print(risk_level)

main()
