import pandas as pd

"""
THIS CODE DOESN'T WORK - I wasn't able to figure it out, so I used other dev's code to solve this challenge.

Order of logic:

- Determine coordinates of all low points in the array
- For each low point:
    - Add the point to basin count
    - Determine surrounding points
    - For each surrounding point:
        - Check it's surrounding points
        - If any of those are lower, AND they aren't in the basin, this point isn't in the basin
    - For any that are, repeat this process
"""


def rewrite_file():
    with open('./input9.txt', 'r') as f:
        lines = f.read().split("\n")
    newlines = []
    for line in lines:
        newline = ''
        for num in line:
            newline += num + ','
        # print(newline)
        newlines.append(newline[:-1])

    with open('./input9.csv', 'w') as newfile:
        newfile.write("\n".join(newlines))


rewrite_file()


def read_file():

    heightmap_array = pd.read_csv('./input9.csv', header=None)

    return heightmap_array


def basin_check(surrounding_point, point, heightmap):
    surrounding_coords = find_surrounding_coords(surrounding_point, heightmap)

    for coord in surrounding_coords:
        if heightmap.iat[coord[0], coord[1]] <= heightmap.iat[point[0], point[1]]:
            if coord == point:
                return False

    return True


def find_surrounding_coords(coordinate, heightmap):
    surrounding_points = []
    current_row = coordinate[0]
    current_column = coordinate[1]

    row_max_index = len(heightmap) - 1
    column_max_index = len(heightmap.columns) - 1

    if current_row == 0:
        pass
    else:
        surrounding_points.append([current_row - 1, current_column])

    if current_row == row_max_index:
        pass
    else:
        surrounding_points.append([current_row + 1, current_column])
    # try:
    #     surrounding_points.append(heightmap.iat[current_row + 1, current_column])
    # except IndexError:
    #     pass

    if current_column == 0:
        pass
    else:
        surrounding_points.append([current_row, current_column - 1])

    if current_column == column_max_index:
        pass
    else:
        surrounding_points.append([current_row, current_column + 1])
    # try:
    #     surrounding_points.append(current_row, current_column + 1])
    # except IndexError:
    #     pass

    return surrounding_points


def find_low_points():

    low_points = []

    heightmap_array = read_file()

    row_index = len(heightmap_array)
    column_index = len(heightmap_array.columns)

    for current_row in range(0, row_index):
        for current_column in range(0, column_index):

            is_low_point = True

            current_value = heightmap_array.iat[current_row, current_column]

            if current_row == 0:
                # print("Top row")
                pass
            else:
                north_value = heightmap_array.iat[current_row-1, current_column]
                if current_value >= north_value:
                    is_low_point = False
                # print(f"N: {north_value}")

            try:
                south_value = heightmap_array.iat[current_row+1, current_column]
                if current_value >= south_value:
                    is_low_point = False
                # print(f"S: {south_value}")
            except IndexError:
                # print("Bottom row")
                pass

            if current_column == 0:
                # print("Left column")
                pass
            else:
                west_value = heightmap_array.iat[current_row, current_column-1]
                if current_value >= west_value:
                    is_low_point = False
                # print(f"W: {west_value}")

            try:
                east_value = heightmap_array.iat[current_row, current_column+1]
                if current_value >= east_value:
                    is_low_point = False
                # print(f"E: {east_value}")
            except IndexError:
                # print("Right column")
                pass

            if is_low_point:
                low_points.append([current_row, current_column])

    return low_points, heightmap_array


def main():

    low_points_array, heightmap_array = find_low_points()

    basin_sizes = []

    count = 0

    for low_point_coord in low_points_array:
        basin_points = [low_point_coord]

        count += 1

        for point in basin_points:

            surrounding_coords = find_surrounding_coords(point, heightmap_array)

            for surrounding_coord in surrounding_coords:
                # If any of the coordinates around the surrounding coord are lower, AND that coord isn't already in
                # the basin, don't add it to the basin
                surrounding_coord_surrounding_coords = find_surrounding_coords(surrounding_coord, heightmap_array)
                for surrounding_coord_surrounding_coord in surrounding_coord_surrounding_coords:
                    if (surrounding_coord_surrounding_coord < surrounding_coord) \
                            and (surrounding_coord_surrounding_coord not in basin_points):
                        pass
                    else:
                        basin_points.append(surrounding_coord)

                if (heightmap_array.iat[surrounding_coord[0], surrounding_coord[1]]\
                        == heightmap_array.iat[point[0], point[1]] + 1) \
                        & (surrounding_coord not in basin_points) \
                        & (heightmap_array.iat[surrounding_coord[0], surrounding_coord[1]] != 9):
                    basin_points.append(surrounding_coord)


        basin_sizes.append(len(basin_points))

    basin_sizes.sort(reverse=True)
    print(basin_sizes)






main()
