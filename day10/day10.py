def open_file():
    with open('./input10.txt', 'r') as f:
        return f.read().split("\n")


def main():
    file = open_file()

    legal_pairs = {
        '(': ')',
        '[': ']',
        '{': '}',
        '<': '>'
    }

    values = {
        ')': 3,
        ']': 57,
        '}': 1197,
        '>': 25137
    }

    syntax_error_score = 0

    for line in file:
        chunk = []
        for character in line:
            if character in legal_pairs.keys():
                chunk.append(character)
            else:
                if legal_pairs[chunk[-1]] != character:
                    print(f"Expected {legal_pairs[chunk[-1]]}, but found {character} instead.")
                    syntax_error_score += values[character]
                    break
                else:
                    chunk = chunk[:-1]

    print(syntax_error_score)


main()
