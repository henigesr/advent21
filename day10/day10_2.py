def rewrite_file():
    with open('./input9.txt', 'r') as f:
        lines = f.read().split("\n")
    newlines = []
    for line in lines:
        newline = ''
        for num in line:
            newline += num + ','
        # print(newline)
        newlines.append(newline[:-1])

    with open('./input9.csv', 'w') as newfile:
        newfile.write("\n".join(newlines))


rewrite_file()


def read_file():

    heightmap_array = pd.read_csv('./input9.csv', header=None)

    return heightmap_array


def main():
    file = open_file()

    legal_pairs = {
        '(': ')',
        '[': ']',
        '{': '}',
        '<': '>'
    }

    values = {
        ')': 3,
        ']': 57,
        '}': 1197,
        '>': 25137
    }

    completion_values = {
        '(': 1,
        '[': 2,
        '{': 3,
        '<': 4
    }

    completion_score_list = []

    syntax_error_score = 0

    unfinished_chunk_portions = []

    for line in file:
        chunk = []
        for character in line:
            if character in legal_pairs.keys():
                chunk.append(character)
            else:
                if legal_pairs[chunk[-1]] != character:
                    syntax_error_score += values[character]
                    chunk = []
                    break
                else:
                    chunk = chunk[:-1]

        if chunk != []:
            unfinished_chunk_portions.append(chunk)

    for chunk in unfinished_chunk_portions:

        completion_score = 0

        for element in reversed(chunk):
            completion_score = (completion_score * 5) + completion_values[element]

        completion_score_list.append(completion_score)

    completion_score_list.sort()

    print(f"{completion_score_list[int(len(completion_score_list)/2-.5)]} is the completion score")

    print(f"{syntax_error_score} is the syntax error score")


main()