import pandas as pd

"""
Order of events:
- First, the energy level of each octopus increases by 1.
- Then, any octopus with an energy level greater than 9 flashes. This increases the energy level of all adjacent 
  octopuses by 1, including octopuses that are diagonally adjacent. If this causes an octopus to have an energy level 
  greater than 9, it also flashes. This process continues as long as new octopuses keep having their energy level 
  increased beyond 9. (An octopus can only flash at most once per step.)
- Finally, any octopus that flashed during this step has its energy level set to 0, as it used all of its energy 
  to flash.
"""


def rewrite_file():
    with open('./input11.txt', 'r') as f:
        lines = f.read().split("\n")
    newlines = []
    for line in lines:
        newline = ''
        for num in line:
            newline += num + ','
        newlines.append(newline[:-1])

    with open('./input11.csv', 'w') as newfile:
        newfile.write("\n".join(newlines))


def read_file():

    heightmap_array = pd.read_csv('./input11.csv', header=None)

    return heightmap_array


def find_surrounding_coords(current_coordinate, octopus_grid):
    row_max = len(octopus_grid) - 1
    column_max = len(octopus_grid.columns) - 1

    surrounding_coords = [[current_coordinate[0]+1, current_coordinate[1]],
           [current_coordinate[0]+1, current_coordinate[1]+1],
           [current_coordinate[0], current_coordinate[1]+1],
           [current_coordinate[0]-1, current_coordinate[1]+1],
           [current_coordinate[0]-1, current_coordinate[1]],
           [current_coordinate[0]-1, current_coordinate[1]-1],
           [current_coordinate[0], current_coordinate[1]-1],
           [current_coordinate[0]+1, current_coordinate[1]-1]]


    if current_coordinate[0] == 0:  # Remove coordinates from the top if the point is in the top
        surrounding_coords = [coordinate for coordinate in surrounding_coords if coordinate[0] != -1]

    if current_coordinate[0] == row_max:  # Remove coordinates from the bottom if the point is in the bottom
        surrounding_coords = [coordinate for coordinate in surrounding_coords if coordinate[0] != row_max+1]

    if current_coordinate[1] == 0:  # Remove coordinates from the left if the point is in the left column
        surrounding_coords = [coordinate for coordinate in surrounding_coords if coordinate[1] != -1]

    if current_coordinate[1] == row_max:  # Remove coordinates from the right if the point is in the right column
        surrounding_coords = [coordinate for coordinate in surrounding_coords if coordinate[1] != column_max+1]

    else:
        pass

    return surrounding_coords


def execute_flash(coordinate, octopus_grid, flashes, current_loop_flashes):

    surrounding_coords = find_surrounding_coords(coordinate, octopus_grid)

    for coord in surrounding_coords:
        octopus_grid.at[coord[0], coord[1]] += 1

    octopus_grid.at[coordinate[0], coordinate[1]] = -20
    flashes += 1
    current_loop_flashes += 1

    return octopus_grid, flashes, current_loop_flashes


def loop_check(octopus_grid):
    row_index = len(octopus_grid)
    column_index = len(octopus_grid.columns)

    for current_row in range(0, row_index):
        for current_column in range(0, column_index):
            if octopus_grid.iat[current_row, current_column] >= 10:
                return True
    return False



def main():
    octopus_grid = read_file()

    flashes = 0

    row_index = len(octopus_grid)
    column_index = len(octopus_grid.columns)

    total_octopi = row_index * column_index

    i = 0

    while True:
        # First, increase the energy level of each octopus by 1
        octopus_grid += 1

        i += 1

        current_loop_flashes = 0

        loop_value = True

        while loop_value:  # Ideally loop through until there aren't
            for current_row in range(0, row_index):
                for current_column in range(0, column_index):
                    if octopus_grid.iat[current_row, current_column] >= 10:
                        # Then, any octopus with an energy level greater than 9 flashes.
                        octopus_grid, flashes, current_loop_flashes = execute_flash([current_row, current_column],
                                                                                    octopus_grid, flashes,
                                                                                    current_loop_flashes)

            loop_value = loop_check(octopus_grid)

        for current_row in range(0, row_index):
            for current_column in range(0, column_index):
                if octopus_grid.iat[current_row, current_column] < 0:
                    octopus_grid.at[current_row, current_column] = 0

        print(f"Loop {i}")
        print(f"Flashes: {current_loop_flashes}/{total_octopi}")
        if current_loop_flashes == total_octopi:
            print(f"Synchronization on loop {i}")
            break


rewrite_file()

main()