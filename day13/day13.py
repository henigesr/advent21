def open_file():
    with open('input13.txt', 'r') as f:
        lines = f.read().splitlines()

    coordinate_list = []

    # Split the file into a list of coordinate tuples, and pull the value for the x and y folds
    temp_coordinate_list = [elem for elem in lines if ',' in elem]
    temp_coordinate_list = [line.split(',') for line in temp_coordinate_list]

    for temp_coord in temp_coordinate_list:
        coordinate_list.append([int(temp_coord[0]), int(temp_coord[1])])

    temp_instructions = [elem for elem in lines if '=' in elem]
    instructions = []

    for temp_instruction in temp_instructions:
        a, b = temp_instruction.split('=')
        axis = a[-1]
        coord = int(b)
        instructions.append([axis, coord])

    return coordinate_list, instructions


def execute_y_fold(coordinates, y_fold):
    # Any coordindates with a y value less than the fold is unchanged.
    # Otherwise, the coordinate's y value after the fold is the value of the line of the fold, minus the difference
    # of the "old" y value and the fold value.
    new_coords = []

    for coordinate in coordinates:
        if coordinate[1] <= y_fold and coordinate not in new_coords:
            new_coords.append(coordinate)
        elif [coordinate[0], (y_fold - (coordinate[1] - y_fold))] not in new_coords:
            new_coords.append([coordinate[0], (y_fold - (coordinate[1] - y_fold))])

    return new_coords


def execute_x_fold(coordinates, x_fold):
    # Any coordindates with a y value less than the fold is unchanged.
    # Otherwise, the coordinate's y value after the fold is the value of the line of the fold, minus the difference
    # of the "old" y value and the fold value.
    new_coords = []

    for coordinate in coordinates:
        if coordinate[0] <= x_fold and coordinate in new_coords:
            pass
        elif coordinate[0] <= x_fold and coordinate not in new_coords:
            new_coords.append(coordinate)
        elif [(x_fold - (coordinate[0] - x_fold)), coordinate[1]] not in new_coords:
            new_coords.append([(x_fold - (coordinate[0] - x_fold)), coordinate[1]])
    return new_coords


def main():
    coordinates, instructions = open_file()

    x_coords = [x for (x, y) in coordinates]
    y_coords = [y for (x, y) in coordinates]


    for instruction in instructions:
        if instruction[0] == 'x':
            coordinates = execute_x_fold(coordinates, instruction[1])
        elif instruction[0] == 'y':
            coordinates = execute_y_fold(coordinates, instruction[1])
        print(len(coordinates))
        print(coordinates)


main()
