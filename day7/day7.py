# Crabs move horizontally
# 1 step costs 1 fuel
# Find the horizontal position that costs the least fuel

def get_crab_positions(test=False):
    """
    Returns a dictionary from the challenge input file, where the key is a horizontal position and the value
    is the count of crabs that are at that position.
    """

    starting_crab_positions = {}

    if test:
        test_pos = [16, 1, 2, 0, 4, 2, 7, 1, 2, 14]
        for position in test_pos:
            starting_crab_positions[position] = test_pos.count(position)
        return starting_crab_positions

    with open('./input7.txt', 'r') as f:
        starting_crabs = [int(i) for i in f.readline().split(',')]

        for position in starting_crabs:
            starting_crab_positions[position] = starting_crabs.count(position)

    return starting_crab_positions


def algorithm(crab_positions):
    """
    Function takes a dictionary of crab positions and counts at that position, and returns the cheapest horizontal
    position to align all crabs on, and the fuel cost

    Where x is an initial horizontal position
    and c is the count of crabs at that given position
    and z is any final position
    and f is the fuel required to move all crabs c from x to z

    f = abs((z-x)) * c
    """

    lowest_position = min(crab_positions.keys())
    highest_position = max(crab_positions.keys())
    print(crab_positions)
    print(highest_position)

    fuel_count_by_position = {}

    for final_position in range(lowest_position, highest_position):
        fuel_count = 0
        for position in crab_positions:
            fuel = abs(final_position - position) * crab_positions[position]
            fuel_count += fuel

        fuel_count_by_position[final_position] = fuel_count
    print(fuel_count_by_position)
    return fuel_count_by_position




def main():

    starting_crab_positions = get_crab_positions()
    fuel_counts = algorithm(starting_crab_positions)
    min_fuel = min(fuel_counts.values())
    print(f"Minimun Fuel Cost: {min_fuel}")
    print(list(fuel_counts.keys())[list(fuel_counts.values()).index(min_fuel)])


main()

